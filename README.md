# autosd-image-soafee

A SOAFEE compliant image.

SOAFEE's architecture specification: https://architecture.docs.soafee.io/en/latest/

## Geting Started

Be sure to create a `output` dir or run `make prepare`.

### Building

Generate a osbuild json file (or run `make compose`):

```sh
automotive-image-builder compose \
	--distro autosd9 \
	--mode package \
	--target qemu \
	image.mpp.yml \
	./output/osbuild.json
```

Build the image (or run `make build`):

```sh
osbuild \
	--store ./output/osbuild \
	--output-directory ./output \
	--export qcow2 \
	./output/osbuild.json
```

## Running

Run the image with `automotive-image-runner`:

```
automotive-image-runner  output/qcow2/disk.qcow2
```

## License

[MIT](./LICENSE)
