SHELL := /bin/bash
AIB_DISTRO := autosd9
AIB_MODE := package
AIB_TARGET := qemu
OUTPUT_DIR := ./output
OSBUILD_JSON_PATH := ${OUTPUT_DIR}/osbuild.json
OSBUILD_STORE_DIR := ${OUTPUT_DIR}/osbuild
OSBUILD_EXPORT := qcow2

.phony: prepare
prepare:
	mkdir -p output

.PHONY: compose
compose:
	automotive-image-builder compose \
		--distro ${AIB_DISTRO} \
		--mode ${AIB_MODE} \
		--target ${AIB_TARGET} \
		image.mpp.yml \
		${OSBUILD_JSON_PATH}

.PHONY: build
build:
	osbuild \
		--store ${OSBUILD_STORE_DIR} \
		--output-directory ${OUTPUT_DIR} \
		--export ${OSBUILD_EXPORT} \
		${OSBUILD_JSON_PATH}
